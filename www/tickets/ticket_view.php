<title>View Tickets</title>
<?php
echo '<link href="styles.css" rel="stylesheet">';

$link=mysqli_connect("","","","");
// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$select = mysqli_query($link,"SELECT tick_id, client_name, contact_name, contact_phone, DATE_FORMAT(tick_open_time, '%W, %m.%d.%y %l:%i %p') AS TIME, tick_desc, tick_note, tick_note2, agent1_name, agent2_name, agent2_note, tick_status FROM ticket WHERE acct_id=1 ORDER BY tick_id DESC");

include("menu.html");

echo "<table width='100%' border='0'>
    <tr>
      <td class='h1'>VIEW TICKETS</td>
    </tr>
  </table>";

echo "<table width='100%' border='1'>

<tr>
<th>Ticket<br>#</th>
<th>Customer<br>Name</th>
<th>Maintenance<br>Status</th>
<th>Contact<br>Info</th>
<th>Ticket<br>Time</th>
<th>Ticket<br>Description</th>
<th>Ticket<br>Solution</th>
<th>Ticket<br>Comment</th>
<th>Tech#2<br>Name</th>
<th>Tech#2<br>Solution</th>
<th>Ticket<br>Status</th>
</tr>";

while($row = mysqli_fetch_array($select)) {
  echo "<tr>";
  echo "<td>" . $row['tick_id'] . "</td>";
  if($row['tick_status'] == 'Open')
  {
  	echo "<td body style='background-color:red'>" . $row['client_name'] . "</td>";
  }
  else
  {
  	echo "<td>" . $row['client_name'] . "</td>";
  }
  echo "<td>" . $row['MAINT_TYPE'] . "</td>";
  echo "<td>" . $row['contact_name'] . "&nbsp;" . $row['contact_phone'] . "</td>";
  echo "<td>" . $row['TIME'] . "</td>";
  echo "<td>" . $row['tick_desc'] . "</td>";
  echo "<td>" . $row['tick_note'] . "</td>";
  echo "<td>" . $row['tick_note2'] . "</td>";
  echo "<td>" . $row['agent2_name'] . "</td>";
  echo "<td>" . $row['agent2_note'] . "</td>";
  echo "<td>" . $row['tick_status'] . "</td>";
  echo "</tr>";
}

echo "</table>";

mysqli_close($link);
?>

