<!DOCTYPE html PUBLIC
"-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<!-- Author: David Silk -->

<html xmlns="http://www.w3.org/1999/xhtml">
	<!-- PHP code begins here -->
	 <?php						//Start PHP code

		// set some basic parameters
		$organization = "Capital Restuarant Systems of Virginia" ;
		$pagetitle = "Payment Portal" ;
		$urlpath = "payments" ;
		$cssfile = "caprestva.css" ;
		$logofile = "logo.jpg" ;
		date_default_timezone_set("America/New_York") ;


		// Function for drawing schedule form
		function draw_payment_form() {

		// start the form and table
		echo '<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">' . "\n" ;
		echo '<input type="hidden" name="cmd" value="_s-xclick">' . "\n" ;
		echo '<input type="hidden" name="hosted_button_id" value="UP4FEYWEK7RDQ">' . "\n" ;
		echo '<table>' . "\n" ;
		echo '<tr><td><input type="hidden" name="on0" value="Support Options">Support Options</td></tr><tr><td><select name="os0">' . "\n" ;
		echo '	<option value="Option 1 - One time">Option 1 - One time $250.00 USD</option>' . "\n" ;
		echo '	<option value="Option 2 - 6 months">Option 2 - 6 months $800.00 USD</option>' . "\n" ;
		echo '	<option value="Option 3 - 1 year">Option 3 - 1 year $1,500.00 USD</option>' . "\n" ;
		echo '</select> </td></tr>' . "\n" ;
		echo '</table>' . "\n" ;
		echo '<input type="hidden" name="currency_code" value="USD">' . "\n" ;
		echo '<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">' . "\n" ;
		echo '<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">' . "\n" ;
		echo '</form>' . "\n" ;

		}

		// Begin page generation
		echo "<head> <title>$organization - $pagetitle</title>\n" ;
		echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"$cssfile\"/> </head>\n" ;
		echo "<body>\n" ;

		echo "<center><img src=\"$logofile\" alt=\"$organization\" /></center><BR />\n" ;

		// connect to the session for this page
		session_start();

		// figure out where this web site is hosted
		$hostname = $_SERVER['HTTP_HOST'] ;
		$servername = $_SERVER['SERVER_NAME'] ;

 		echo "<center><h1>$organization - $pagetitle</h1>" . "\n" ;
 		echo '<BR><BR>' . "\n" ;

		echo '<p align="center"> Thank you for visiting our payments portal.  This service is provided through PayPal Business.' . "\n" ;
		echo 'Please select the support option that best fits your needs.' . "</p>\n" ;
		echo '<BR>' . "\n" ;

		draw_payment_form() ;

	  ?>
	<!-- PHP code ends here -->


	<BR />
	<BR />
	<BR />
	<BR />
	<hr/>
	<p align="center"><i>Capital Restaurant Systems of Virginia<BR />
	(c) Copyright 2016 <BR />
	All Rights Reserved</i></p>
	<hr/>
 </body>
</html>
