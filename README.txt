
Hi Team!

This is our team repository for sharing code.  


You will need software to access Git, but its all free and here are some options:


You can use the SourceTree application on Windows or Mac: https://www.sourcetreeapp.com/download/

If you just want a basic command line Git, download here: https://git-scm.com/downloads

Basic commands can be found here: https://confluence.atlassian.com/bitbucketserver/basic-git-commands-776639767.html 


There are lots of videos on how to do this.  Here are a couple: 

https://www.youtube.com/watch?v=FMW0W2yrmsY
https://www.youtube.com/watch?v=Ojyq-63b5F0 

